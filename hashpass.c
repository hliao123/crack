#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
	//check number of command  line arguments
	
	if (argc < 3)
	{
		fprintf(stderr, "You must supply two files\n");
		
		exit(1);
	}

	//open file for reading
	
	FILE* in;
	
	in = fopen(argv[1], "r");
	
	if (!in)
	{
		fprintf(stderr, "Cannot open %s for reading \n", argv[1]);
		
		exit(2);
	}

	//open file for writing
	
	FILE* out;
	
	out = fopen(argv[2], "w");
	
	if (!out)
	{
		fprintf(stderr, "Cannot open %s for writing \n", argv[2]);
		
		exit(3);
	}
	
	//comparing two files
	
	if (strcmp(argv[1], argv[2]) == 0)
	{
		fprintf(stderr, "The two files cannot be the same");
		
		exit(4);
	}
	
	char line[1000];
	
	//read file line by line
	
	while (fgets(line, 1000, in) != NULL)
	{
	    int oldLength = strlen(line);

		if (line[oldLength - 1] == '\n')
		{
		    line[oldLength - 1] = '\0';
		}
		
		int newLength = strlen(line);
	    
	    char *hash = md5(line, newLength);
	    
	    if (hash != NULL)
	    {
	        fprintf(out, "%s", hash);
	        
	        fprintf(out, "%s", "\n");
	        
	        free (hash);
	    }
	}
	
	//close files
	
	fclose(in);
	fclose(out);
}